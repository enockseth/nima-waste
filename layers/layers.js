var wms_layers = [];


        var lyr_NimaOpenCitiesDroneImage_0 = new ol.layer.Tile({
            'title': 'Nima - Open Cities Drone Image',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tiles.openaerialmap.org/5d2d0ab4f416f40006cffcc1/0/5d2d0ab4f416f40006cffcc2/{z}/{x}/{y}'
            })
        });
var format_NimaSampleData_1 = new ol.format.GeoJSON();
var features_NimaSampleData_1 = format_NimaSampleData_1.readFeatures(json_NimaSampleData_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_NimaSampleData_1 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_NimaSampleData_1.addFeatures(features_NimaSampleData_1);
var lyr_NimaSampleData_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_NimaSampleData_1, 
                style: style_NimaSampleData_1,
                interactive: true,
                title: '<img src="styles/legend/NimaSampleData_1.png" /> Nima Sample Data'
            });

lyr_NimaOpenCitiesDroneImage_0.setVisible(true);lyr_NimaSampleData_1.setVisible(true);
var layersList = [lyr_NimaOpenCitiesDroneImage_0,lyr_NimaSampleData_1];
lyr_NimaSampleData_1.set('fieldAliases', {'id': 'id', '@id': '@id', 'amenity': 'amenity', 'fee': 'fee', 'operator': 'operator', 'wheelchair': 'wheelchair', 'building': 'building', 'addr:city': 'addr:city', 'addr:community': 'addr:community', 'addr:street': 'addr:street', 'addr:suburb': 'addr:suburb', 'building:levels': 'building:levels', 'building:material': 'building:material', 'roof:material': 'roof:material', 'source': 'source', 'female': 'female', 'male': 'male', 'toilets:disposal': 'toilets:disposal', 'toilets:position': 'toilets:position', 'access': 'access', 'unisex': 'unisex', 'survey': 'survey', 'fixme': 'fixme', 'addr:housenumber': 'addr:housenumber', 'name': 'name', 'toilets:handwashing': 'toilets:handwashing', 'waste': 'waste', 'toilets': 'toilets', 'changing_table': 'changing_table', 'natural': 'natural', 'drinking_water': 'drinking_water', 'level': 'level', 'colour': 'colour', 'material': 'material', 'city': 'city', 'community': 'community', });
lyr_NimaSampleData_1.set('fieldImages', {'id': '', '@id': '', 'amenity': '', 'fee': '', 'operator': '', 'wheelchair': '', 'building': '', 'addr:city': '', 'addr:community': '', 'addr:street': '', 'addr:suburb': '', 'building:levels': '', 'building:material': '', 'roof:material': '', 'source': '', 'female': '', 'male': '', 'toilets:disposal': '', 'toilets:position': '', 'access': '', 'unisex': '', 'survey': '', 'fixme': '', 'addr:housenumber': '', 'name': '', 'toilets:handwashing': '', 'waste': '', 'toilets': '', 'changing_table': '', 'natural': '', 'drinking_water': '', 'level': '', 'colour': '', 'material': '', 'city': '', 'community': '', });
lyr_NimaSampleData_1.set('fieldLabels', {'id': 'no label', '@id': 'no label', 'amenity': 'no label', 'fee': 'no label', 'operator': 'no label', 'wheelchair': 'no label', 'building': 'no label', 'addr:city': 'no label', 'addr:community': 'no label', 'addr:street': 'no label', 'addr:suburb': 'no label', 'building:levels': 'no label', 'building:material': 'no label', 'roof:material': 'no label', 'source': 'no label', 'female': 'no label', 'male': 'no label', 'toilets:disposal': 'no label', 'toilets:position': 'no label', 'access': 'no label', 'unisex': 'no label', 'survey': 'no label', 'fixme': 'no label', 'addr:housenumber': 'no label', 'name': 'no label', 'toilets:handwashing': 'no label', 'waste': 'no label', 'toilets': 'no label', 'changing_table': 'no label', 'natural': 'no label', 'drinking_water': 'no label', 'level': 'no label', 'colour': 'no label', 'material': 'no label', 'city': 'no label', 'community': 'no label', });
lyr_NimaSampleData_1.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});